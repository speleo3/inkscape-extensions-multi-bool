=====
Usage
=====

* Create the top path just like you would for doing a normal cut / difference / division in Inkscape.
* Select all objects (paths or shapes only, including the top path) that you want to use the extension with, then go to Extensions -> Boolean -> Multiple Cut / Multiple Difference / Multiple Division.
* Wait a little for Inkscape to perform the operation.
* Done!

=========================================================
Inkscape Multi-Bool Extension Manual
=========================================================

About
=====

.. image:: images/multibool.png
   :alt: Multi-Bool Overview
   :width: 30%
   :align: left

Fork of Ryan Lerch's `multiple-difference Inkscape extension
<https://github.com/ryanlerch/inkscape-extension-multiple-difference>`_.

A set of three inkscape extensions to apply

-    Path -> Difference
-    Path -> Cut Path
-    Path -> Division

on multiple selected paths or shapes at once.

The *topmost* object is used to cut / divide / do a difference on *each selected object* below.

In contrast to the standard operations, it *keeps* the topmost object.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   installation
   usage
   known_issues
   licence
   contributing

============
Contributing
============

If you would like to contribute to the development of this extension, you can!

* `Report bugs <https://gitlab.com/Moini/inkscape-extensions-multi-bool/issues>`_
* Work on the `code <https://gitlab.com/Moini/inkscape-extensions-multi-bool/tree/master>`_
